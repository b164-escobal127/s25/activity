//2

db.fruits.aggregate([
    { $unwind: "$origin" },
    { $project: {name: 1, _id: 0} }
])


//2

db.fruits.aggregate([
    {$match: { onSale: true } },
    {$count: "fruitsOnsale"}
  ])

//3

db.fruits.aggregate([
    {$match: {stock: {$gte:20}}},
    {$count: "fruitsStocks"}
])

//4

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: { _id: "$supplier_id", avg: { $avg: "$price" } } }
])

//5

db.fruits.aggregate([{$match: { onSale: true }},
    {
       $group:
         {
           _id: "$supplier_id",
           maxPrice: {$max: "$price"} }
         }
])

//6

db.fruits.aggregate([
        {
       $group:
         {
           _id: "$supplier_id",
           minPrice: {$min: "$price"} }
         }
])
